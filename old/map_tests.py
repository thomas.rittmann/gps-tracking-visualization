import numpy as np
import cartopy.crs as ccrs
import csv
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
#plt.ion()
fig=plt.figure(figsize=(10,10))
ax = plt.axes(projection=ccrs.EuroPP())
ax.set_extent([-10,30,20,90],crs=ccrs.Geodetic())
ax.coastlines()
ax.add_feature(cfeature.LAND)
ax.add_feature(cfeature.OCEAN)
ax.add_feature(cfeature.BORDERS,linestyle=':')
ax.add_feature(cfeature.LAKES)
ax.add_feature(cfeature.RIVERS)
tit=plt.title('Bewegungsprofil Freddi & Fritz 2018')
tit.set_fontsize(18)
with open('Bewegungsprofil_v01.csv') as datafile:
    fullrows=csv.reader(datafile,delimiter=';',quotechar='|')
    x_coord=list();
    y_coord = list();
    x_coord_f = list();
    y_coord_f = list();
    timestamp=list();
    row_count=0
    counter=0
    #plt.ion()
    for row in fullrows:
        row_count +=1
        if row_count>1:
            print(row[0])
            timestamp.append(row[0])
            x_coord.append(float(row[5]))
            y_coord.append(float(row[6]))
            #if (float(row[7])!=0):
            x_coord_f.append(float(row[7]))
            y_coord_f.append(float(row[8]))
            #counter+=1


            if row_count>2:
                if row_count>3:
                    points.remove()
                    points_fritz.remove()
                    del points
                    del points_fritz
                points=plt.scatter(float(row[6]),float(row[5]), c='black',s=40, alpha=1, transform=ccrs.Geodetic())
                points_fritz=plt.scatter(float(row[8]),float(row[7]), c='green',s=40, alpha=1, transform=ccrs.Geodetic())
                #allpts=plt.scatter(float(row[6]),float(row[5]), c='red',s=10, alpha=1, transform=ccrs.Geodetic())
                plt.plot(y_coord[row_count-3:row_count+10], x_coord[row_count-3:row_count+10], linestyle='--',c='red', linewidth=1, transform=ccrs.Geodetic())#
                plt.plot(y_coord_f[row_count - 3:row_count + 10], x_coord_f[row_count - 3:row_count + 10], linestyle='--',
                         c='blue', linewidth=1, transform=ccrs.Geodetic())
                plt.draw()
                plt.pause(0.01)
                if row_count>3:
                    date_field.remove()
                date_field = plt.text(-9, 33, row[0], fontsize=20, transform=ccrs.Geodetic(),
                                      bbox=dict(facecolor='white', alpha=1.0))
plt.show()
#plt.scatter(y_coord,x_coord,s=30,alpha=1,transform=ccrs.Geodetic())
#plt.plot(y_coord,x_coord,linestyle='--',linewidth=1,transform=ccrs.Geodetic())
#plt.show()