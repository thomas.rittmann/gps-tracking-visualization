#!/bin/bash
rm $1
exiftool -CSV -CreateDate -GPSLatitude -GPSLongitude -c "%.8f" -d "%Y-%m-%d,%H:%M:%S" ./* >> $1
sed -i -e 's/ W/,W/g' $1
sed -i -e 's/ E/,E/g' $1
sed -i -e 's/ S/,S/g' $1
sed -i -e 's/ N/,N/g' $1
sed -i -e 's/"//g' $1
sed -i '/SourceFile/d' $1
