#!/usr/bin/python

import csv
from math import sqrt,radians, sin,cos, atan2
import getopt, sys

def gps_distance(lat1,lon1,lat2,lon2):
	lat1=radians(lat1)
	lon1=radians(lon1)
	lat2=radians(lat2)
	lon2=radians(lon2)
	R=6373
	dlon=lon2-lon1
	dlat=lat2-lat1
	a=sin(dlat/2)**2+cos(lat1)*cos(lat2)*sin(dlon/2)**2
	c=2*atan2(sqrt(a),sqrt(1-a))
	calculated = R*c
	return calculated


datalist=list()
inputfile=''
outputfile=''
try:
	opts, args = getopt.getopt(sys.argv[1:],"hi:o:",["ifile=","ofile="])
	print(opts)
except getopt.GetoptError:
	print('test.py -i <inputfile> -o <outputfile>')
	sys.exit(2)
for opt, arg in opts:
	if opt == '-h':
		print('test.py -i <inputfile> -o <outputfile>')
		sys.exit()
	elif opt in ("-i", "--ifile"):
		inputfile = arg
	elif opt in ("-o", "--ofile"):
		outputfile = arg
with open (inputfile) as datafile:
	rawdata=csv.reader(datafile,delimiter=',')
	rowcount=0
	for ro in rawdata:
		if ro[3]!='':
			ro[3]=float(ro[3])
			ro[5]=float(ro[5])
			if ro[4]=='S':
				ro[3]=-ro[3]
			if ro[6]=='W':
				ro[5]=-ro[5]
			ro.pop(4)
			ro.pop(5)
			datalist.append(ro)

	datalist.sort(key=lambda r: (r[1], r[2]))
	oldrow=datalist[10]
	with open (outputfile, 'w') as writefile:
		filewriter=csv.writer(writefile,delimiter=';')
		for ro in datalist:
			rowcount+=1
			dist=gps_distance(float(oldrow[3]),float(oldrow[4]),float(ro[3]),float(ro[4]))
			if dist>20 or ro[1]!=oldrow[1]:
#				print(dist)
				print (ro)
				filewriter.writerow(ro)				
			oldrow=ro

