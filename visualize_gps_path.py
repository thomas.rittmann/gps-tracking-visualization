import numpy as np
import cartopy.crs as ccrs
import csv
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import os

fig=plt.figure(figsize=(10,10))
ax = plt.axes(projection=ccrs.EuroPP())
ax.set_extent([-10,30,10,90],crs=ccrs.Geodetic())

ax.coastlines()
ax.add_feature(cfeature.LAND)
ax.add_feature(cfeature.OCEAN)
ax.add_feature(cfeature.BORDERS,linestyle=':')
ax.add_feature(cfeature.LAKES)
ax.add_feature(cfeature.RIVERS)
tit=plt.title('Movement Profile Frederik 2021')
tit.set_fontsize(18)

with open('fred2021.csv') as datafile:
    fullrows=csv.reader(datafile,delimiter=';',quotechar='|')
    x_coord=list();
    y_coord = list();
    x_coord_f = list();
    y_coord_f = list();
    timestamp=list();
    gif_ims=list();
    row_count=0
    counter=0
    #plt.ion()
    for row in fullrows:
        row_count +=1
        if row_count>0:
            print(row[1])
            timestamp.append(row[1])
            x_coord.append(float(row[3]))
            y_coord.append(float(row[4]))

            if row_count>0:
                if row_count>1:
                    points.remove()
                    #points_fritz.remove()
                    del points
                    #del points_fritz

                plt.plot(y_coord[row_count-2:row_count], x_coord[row_count-2:row_count], linestyle='--',c='red', linewidth=1, transform=ccrs.Geodetic())#
                points = plt.scatter(float(row[4]), float(row[3]), c='black', s=40, alpha=1, transform=ccrs.Geodetic(),zorder=3)
                #plt.plot(y_coord_f[row_count - 3:row_count + 10], x_coord_f[row_count - 3:row_count + 10], linestyle='--',
                        # c='blue', linewidth=1, transform=ccrs.Geodetic())
                if row_count>3:
                    date_field.remove()
                date_field = plt.text(-9, 33, row[1], fontsize=20, transform=ccrs.Geodetic(),
                                      bbox=dict(facecolor='white', alpha=1.0))
                plt.draw()
                plt.pause(0.01)
                imgstring = f'pic{row_count:04d}'
                savestring = os.path.join('pics_2021', imgstring)
                fig.savefig(savestring)
plt.show()