# GPS Tracking Visualization

This tool allows for an easy and fast creation of a long-term movement profile based on raw exif-gps data from your images taken with a smartphone. The profile can subsequently be vizualized in a "map movie". 

Requirements: 
- Python distribution with the packages *numpy, cartopy, csv, math, matplotlib, os*
- any unix-like shell to run the batch scripts
- the commandline tools "exiftool" and "ffmpeg". often preinstalled, otherwise install via apt-get, brew, etc.

Procedure:
1. Run "exif2gps.sh <yourcsv.csv>" in the folder with your pictures to write all available time stamps and gps coordinates into a .csv file. 
2. Merge the csv files from several batches if necessary.
3. Sort the coordinates by using "sortcoordinates.py -i <inputcsv> -o <outputcsv>'". For clarity and visualization purposes, this tool keeps only one coordinate per day, as long as no movement larger than 10km occured. 
4. Inspect the final csv, it should be in the format <name>,<date>,<time>,<latitude>,<longitude>.
5. From this csv you can create a map frame (as .png) per day with "visualize_gps_path.py". Each frame will include the timestamp, current gps position and recorded path so far. Adjust the map boundaries for your needs (currently set for western europe). Be aware of the large size of these raw images.
6. Use "framestomovie.sh" to create a well compressed movie clip (.mp4) from the raw frames. Due to the small image changes the size should be ~10MB only. 
7. Be amazed by all the places you visited but had already forgotten about!